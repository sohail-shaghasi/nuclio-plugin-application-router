<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\router
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\http\uri\URI;
	use nuclio\plugin\http\request\Request;
	use nuclio\plugin\config\Config;
	use nuclio\plugin\config\ConfigCollection;
	/**
	 * Router plugin
	 * 
	 * This plugin is used by application to respond to incoming requests, it will analyze the request and match it with the available routes to invoke the functions based on routes.
	 * 
	 * @package    nuclio\plugin\application\router
	 */
	<<factory>>
	class Router extends Plugin
	{
		/**
		 * @property Vector<string> $routes list of routes that were mapped using the application or using the Routing +Service
		 * @access public
		 */
		public Vector<string> $routes 		=Vector{};
		/**
		 * @property Vector<string> $controller list of controllers that may be linked to a route.
		 * @access public
		 */
		public Vector<string> $controller 	=Vector{};
		/**
		 * @property Vector<string> $methods list of methods that may be called once a route is invoked (GET,POST,OPTION... etc).
		 * @access public
		 */
		public Vector<string> $methods 		=Vector{};
		/**
		 * @property Vector<mixed> $callbacks list of actions assosiated with a route.
		 * @access public
		 */
		public Vector<mixed> $callbacks		=Vector{};
		/**
		 * @property Map<string,string> $patterns
		 * @access public
		 */
		public Map<string,string> $patterns =Map
		{
			':any...'	=>'.+',
			':any'		=>'[^/]+',
			':int'		=>'[0-9_-]+',
			':string'	=>'[a-z_-]+'
		};
		/**
		 * @property Vector<string> $error_callback.
		 * @access public
		 * @static
		 */
		public static Vector<string> $error_callback=Vector{};
		private Config $config;
		private string $application;
		private string $namespace;
		private URI $URI;
		private Request $request;
		private int $pointer;
		private string $controllerDir;
		private bool $gotRoute=false;

		/**
		 * Get the instance.
		 *
		 * @param      ...     $args   argument to be sent to the constructor (string $application,URI $URI,Config $config) {@link __construct()}
		 *
		 * @return     Router  Instance.
		 * @access public
		 * @static
		 */
		public static function getInstance(/* HH_FIXME[4033] */ ...$args):Router
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
		 * constructer function.
		 * 
		 * Build the router object and initialize request object.
		 * 
		 * @param      string  $application  application to manage the routing for it.
		 * @param      URI     $URI          the URI object for the invoker application.
		 * @param      Config  $config       the config for the invoker application.
		 * @access public
		 */
		public function __construct(string $application,URI $URI,Config $config)
		{
			$this->application	=$application;
			$this->config		=$config;
			$this->URI			=$URI;
			$this->request		=Request::getInstance();
			$this->pointer		=1;
			$parts				=explode('\\',$this->application);
			array_pop($parts);
			$this->namespace	=implode('\\',$parts);
			$this->controllerDir=ROOT_DIR.str_replace('\\','/',$this->namespace).'/controller/';
			parent::__construct();
		}
		
		/**
		 * Add new route.
		 * 
		 * Add new route information to the list of routes along with the methods and the related callbacks.
		 * 
		 * @param  string $uri      URI of the path 
		 * @param  mixed $callback  Function to be trigger or "controller@action".
		 * @param  string $method   Request method, GET/POST etc.
		 * @access public
		 */
		public function newRoute(string $uri, mixed $callback, string $method):void
		{
			$this->routes->add($uri);
			$this->methods->add(strtoupper($method));
			$this->callbacks->add($callback);
		}

		/**
		 * execute the current request against the collection of routes.
		 * 
		 * Execute the collection of route, if route does not exist, throw 404 page. Support regex and simple routing.
		 * 
		 * @param  string $execUri       Uri of path being executed.
		 * @param  string $requestMethod Request method of the method being executed.
		 * @access public
		 */
		public function execute():void
		{
			$autoRoute	=$this->config->get('router.autoRoute');
			$callback	=$this->callbacks->toArray();
			$uri		=$this->URI->getFull();
			$method		=$this->request->getMethod();
			$searches	=array_keys($this->patterns);
			$replaces	=array_values($this->patterns);
			$route_exist=false;
			$path		='';
			
			if ($autoRoute=='true')
			{
				//handle autoroute
				$controller=$this->URI->getFirst();
				$method=$this->URI->getPart(1);

				if (!$controller)
				{
					$this->doRoute('Index','_index',Map{});
				}
				else
				{
					$control=$controller;
					
					$action	='_index';
					$args	=[];
					$node=$method;
					if (!is_null($node))
					{
						$action	=$method;
						$node	=$this->pointer+1;
						if (!is_null($node))
						{
							while (true)
							{
								$arg = $this->URI->getPart($node++);
								if(is_null($arg))
								{
									break;
								}
								$args[] = $arg;
							}
						}
					}
					$args=new Map($args);
					$this->doRoute($control, $action, $args);
				}
			}
			else
			{
				//For manual configured routing.
				$end	=end(explode('/',$uri));
				$format	=end(explode('.',$end));
				$uri	=str_replace('.'.$format,'', $uri);
				
				if (in_array($uri, $this->routes))
				{
					$route_index = array_keys($this->routes, $uri);
					foreach ($route_index as $route)
					{
						if ($this->methods[$route] == $method)
						{
							$route_exist = true;
							if(!is_object($callback[$route]))
							{
								$path		=$this->buildPath($route);
								$extendPath	=$this->extendPath($route);
								$parts		=explode('/',$callback[$route]);
								$subFolder	=$parts;
								array_pop($subFolder);
								$subFolder	=implode('\\',$subFolder);
								$last		=end($parts);
								list($controller,$controllerAction) = explode('@',$last);
								// $fullPath=$extendPath.ucfirst($controller);
								// $checkPath=$path.ucfirst($controller).'.hh';
								
								if(class_exists($controller,true))
								{
									$controllerInstance=ClassManager::getClassInstance
									(
										$controller,
										$this->URI,
										$this->config->get('controller')
									);
									if (method_exists($controllerInstance,$controllerAction))
									{
										$this->gotRoute=true;
										call_user_func_array([$controllerInstance, $controllerAction], $args=[]);
									}
									else
									{
										throw new RouterException(sprintf('Invalid route! Routing action "%s" does not exist in class "%s".',$controllerAction,$controller),500);
									}
								}
								else
								{
									$this->controllerNotFound($controller);
								}
							}
							else
							{
								call_user_func($callback[$route]);
							}
						}
						else
						{
							$this->methodNotSupported($this->methods[$route]);
						}
					}
				}
				else
				{
					$index = 0;
					$args = [];
					foreach ($this->routes as $route)
					{
						if (strpos($route, ':') !== false)
						{
							$route = str_replace($searches, $replaces, $route);
						}
						if (preg_match('#^' . $route . '$#', $uri, $args))
						{
							if ($this->methods[$index] == $method)
							{
								$route_exist=true;
								array_shift($args);
								if (!is_object($this->callbacks[$index]))
								{
									$path		=$this->buildPath($index);
									$extendPath	=$this->extendPath($index);
									$parts		=explode('/',$callback[$index]);
									$subFolder	=$parts;
									array_pop($subFolder);
									$subFolder	=implode('\\',$subFolder);
									$last		=end($parts);
									list($controller,$controllerAction) = explode('@',$last);
									// $fullPath=$extendPath.ucfirst($controller);
									// $checkPath=$path.ucfirst($controller).'.hh';
									
									if (class_exists($controller,true))
									{
										$controllerInstance=ClassManager::getClassInstance
										(
											$controller,
											$this->URI,
											$this->config->get('controller')
										);
										if (method_exists($controllerInstance,$controllerAction))
										{
											$this->gotRoute=true;
											call_user_func_array([$controllerInstance, $controllerAction], $args);
										}
										else
										{
											throw new RouterException(sprintf('Invalid route! Routing action "%s" does not exist in class "%s".',$controllerAction,$controller),500);
										}
									}
									else
									{
										$this->controllerNotFound($controller);
									}
								}
								else
								{
									call_user_func($callback[$index]);
								}
							}
							else
							{
								$this->methodNotSupported($this->methods[$index]);
							}
						}
						$index++;
					}
				}
				if (!$route_exist)
				{
					$this->UrlNotFound();
				}
			}
		}
		
		/**
		 * Determine capability to route to.
		 * 
		 * check to see if the url exists in the list of available routes, it checks the uri against internal routes list by using {@link findMatchingRoutes()}.
		 * 
		 * @param      string   $URI    (description)
		 * @return     boolean  True if capable to route to, False otherwise.
		 * @internal   this function uses the public method {@link findMatchingRoutes()}.
		 * @access public
		 */
		public function canRouteTo(string $URI):bool
		{
			$result=$this->findMatchingRoutes($URI);
			if ($result->count())
			{
				return true;
			}
			return false;
		}
		
		/**
		 * check a url against the internal routes list and return a list of all matches.
		 * @param      string  $uri    string that contains the url that you want to check against the internal list of routes.
		 * @return     Vector<Map<string,mixed>>  list of routes which will look like this :
		 * 		Map
		 *			{
		 *				'path'	=>"the url for the route",
		 *				'action'=>"the action related to the route",
		 *				'method'=>"the request method related to the route"
		 *			}
		 * @access public
		 */
		public function findMatchingRoutes(string $uri):Vector<Map<string,mixed>>
		{
			$foundRoutes	=Vector{};
			$autoRoute		=$this->config->get('router.autoRoute');
			$callback		=$this->callbacks->toArray();
			$method			=$this->request->getMethod();
			$searches		=array_keys($this->patterns);
			$replaces		=array_values($this->patterns);
			$path			='';
			
			if ($autoRoute=='true')
			{
				//TODO findMatchingRoute with autoRoute set to true.
				die('TODO: findMatchingRoute with autoRoute set to true.');
			}
			else
			{
				//For manual configured routing.
				$end	=end(explode('/',$uri));
				$format	=end(explode('.',$end));
				$uri	=str_replace('.'.$format,'', $uri);
				
				if (in_array($uri, $this->routes))
				{
					$routeIndex=array_keys($this->routes, $uri);
					foreach ($routeIndex as $route)
					{
						if ($this->methods[$route]==$method)
						{
							$foundRoutes->add
							(
								Map
								{
									'path'	=>$this->routes[$route],
									'action'=>$this->callbacks[$route],
									'method'=>$method
								}
							);
						}
					}
				}
				else
				{
					$index = 0;
					$args = [];
					foreach ($this->routes as $route)
					{
						if (strpos($route, ':') !== false)
						{
							$route = str_replace($searches, $replaces, $route);
						}
						if (preg_match('#^' . $route . '$#', $uri, $args))
						{
							if ($this->methods[$index] == $method)
							{
								$foundRoutes->add
								(
									Map
									{
										'path'	=>$route,
										'action'=>$this->callbacks[$index],
										'method'=>$this->methods[$index]
									}
								);
							}
						}
						$index++;
					}
				}
			}
			return $foundRoutes;
		}
		
		/**
		 * Handle simple routing. Mostly used for autoroute.
		 * 
		 * @param  string|null $control Controller to be called
		 * @param  string|null $action  Action to be run (function).
		 * @param  mixed  $args    List of arguments.
		 * @access public
		 */
		public function doRoute(?string $control, ?string $action, ?Map<int,string> $args=null):void
		{
			$control=ucfirst($control);
			if (!is_null($args))
			{
				$args=$args->toArray();
			}
			else
			{
				$args=[];
			}
			if(!file_exists($this->controllerDir.$control.'.hh'))
			{
				$this->UrlNotFound();
			}
			else
			{
				$fullPath=$this->namespace.'\\controller\\'.$control;
				$controller=ClassManager::getClassInstance
				(
					$fullPath,
					$this->URI,
					$this->config->get('controller')
				);
				call_user_func_array([$controller,$action], $args);
				$this->gotRoute=true;
			}
		}
		
		/**
		 * @depricated
		 * @see Router::doRoute()
		 */
		public function handleSimple(?string $control, ?string $action, ?Map<int,string> $args=null):void
		{
			$args=func_get_args();
			return $this->doRoute(...$args);
		}

		/**
		 * Build path to the controller.
		 * 
		 * @param  string $route Index of current execution path.
		 * @return string        Path to the controller that should be called.
		 * @access public
		 */
		public function buildPath(int $route):string
		{
			$callback	=$this->callbacks->toArray();
			$parts		=explode('/',$callback[$route]);
			array_pop($parts);
			if(count($parts)==0)
			{
				$path=$this->controllerDir;
			}
			else
			{
				$path=$this->controllerDir.implode('/',$parts).'/';
			}
			return $path;
		}

		/**
		 * Try to extend the path to controllers by adding the route path parts to the controllers path.
		 * 
		 * @param  string $route Index of current execution path.
		 * @return      string the extended path to the controller.
		 * @access public
		 */
		public function extendPath(int $route):string
		{
			$callback	=$this->callbacks->toArray();
			$parts		=explode('/',$callback[$route]);
			array_pop($parts);
			if(count($parts)==0)
			{
				$path=$this->controllerDir;
			}
			else
			{
				$path=$this->controllerDir.implode('/',$parts).'\\';
			}
			return $path;
		}
		
		/**
		 * check whether the route has been executed.
		 *
		 * @return     bool true in case the route has been executed using {@link execute()} or {@link doRoute()}
		 * @access public
		 */
		public function gotRoute():bool
		{
			return $this->gotRoute;
		}

		/**
		 * URL NOT FOUND handler
		 * 
		 * @throws     RouterException throw a 404 error.
		 * @access private
		 */
		private function UrlNotFound():void
		{
			throw new RouterException('Page not found.',404);
		}

		/**
		 * Controller not found handler
		 * 
		 * @throws     RouterException throw an exception in case the controller is not found.
		 * @access private
		 */
		private function controllerNotFound(string $controller):void
		{
			throw new RouterException(sprintf('Controller "%s" could not be found.',$controller),500);
		}

		/**
		 * Method not supported exception.
		 * 
		 * @throws     RouterException throw an exception in case the request method is not supported.
		 * @access private
		 */
		private function methodNotSupported(string $method):void
		{
			throw new RouterException(sprintf('Request method "%s" is not supported.',$method),405);
		}
	}
}
